
<div align="center">
<br>
<img width="350" src="./docs/.vuepress/images/electron-nuxt.png" alt="electron-nuxt">
<br>
<br>
</div>

<div align="center">



## Cara Instal

```bash
# Install vue-cli and scaffold boilerplate
npm install -g vue-cli
vue init michalzaq12/electron-nuxt <project-name>

# Install dependencies and run your app
cd <project-name>
yarn install
yarn run dev 
```

**Take a look at the [documentation](https://michalzaq12.github.io/electron-nuxt/). Here you will find useful information about configuration, project structure, and building your app**

## Sponsors

*Electron-nuxt is an MIT licensed open source project and completely free to use. However, 
if you run a business and are using Electron-nuxt in a revenue-generating product, 
it makes business sense to sponsor project development.*


## Contoh

* **NKNxVault**: NKN desktop wallet (https://github.com/rule110-io/vault)
* [**Reflex**](https://reflexapp.nickwittwer.com): Responsive web browser for developers (https://github.com/nwittwer/reflex)
* **BinaryBotPlayground**: An Electron app for loading and running Binary bots (https://github.com/gabriellanzer/BinaryBotPlayground)
* **system-companion**: Multi platform app for getting system information (https://github.com/romslf/system-companion)
* **NSMultiTools**: Graphical interface to make life easier for Nintendo Switch hackers (https://github.com/MeatReed/NSMultiTools)
* **blue-burlap**: CI/CD For Salesforce Deployments (https://github.com/fuzzybaird/blue-burlap)
* [**cuesync**](https://cuesync.pro/): Synchronize cues between Algoriddim Djay, Serato DJ Lite/Pro and Virtual DJ (https://github.com/schneefux/cuesync)
* **VKGram**: Messenger for VK that allows you to send custom stickers (https://github.com/PurpleHorrorRus/VKGram)
* **twitch-vip-controller**: An app to help Twitch streamers automate VIP redemption and removal (https://github.com/kisuka/twitch-vip-controller)
* [**Keyanu**](https://cloud.kopanko.com/index.php/s/t7karHgpWLqdinA): Keyboard shortcut manager (https://github.com/pcktm/Keyanu)
* [**JCloisterZone**](https://jcloisterzone.com/en/): PC implementation of Carcassonne board game (https://github.com/farin/JCloisterZone-Client)
* **StreamOverlay**: An overlay for streamers with single display (https://github.com/PurpleHorrorRus/StreamOverlay)
